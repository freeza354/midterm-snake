#pragma once
class Food
{
public:
	~Food();
	Food(char shape, int type, int givenChild);
	int typeGetter();
	int givenChildGetter();
	char shapeGetter();

private:
	char shape_food;
	int type_food;
	int give_Child;

};

