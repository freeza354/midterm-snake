#pragma once
class Maps
{
public:
	Maps();
	~Maps();
	void createMap(int x, int y);
	void inputCharWall(char tembok);
	void createWall(int posX, int posY);
	void showMap();
	void createWallCollision();
	void inputObjectIntoMap(char object,int posX, int posY);

private:
	char wall;
	int size_x, size_y;
	char **arrayWall = new char*[size_y];
	int **arrayCollision = new int*[size_y];

};

