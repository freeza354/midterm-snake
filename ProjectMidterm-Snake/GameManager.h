#pragma once
class GameManager
{

	enum {UP, DOWN, LEFT, RIGHT} dir;

public:
	GameManager();
	~GameManager();
	void printAll();
	void get_input(const Maps & mapInput);
	void move_snake(Maps map, Player snake);
	void inputIntoMap(Maps map,char object, int posX, int posY);

private:
	int positionX, positionY;

};

