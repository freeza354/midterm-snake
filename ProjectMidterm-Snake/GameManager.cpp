#include "pch.h"
#include "GameManager.h"
#include "Food.h"
#include "Player.h"
#include "Maps.h"
#include <Windows.h>


GameManager::GameManager()
{
}


GameManager::~GameManager()
{
}

void GameManager::get_input(const Maps & mapInput) {
	if (GetAsyncKeyState(VK_UP) && dir != DOWN)
	{
		dir = UP;
	}
	if (GetAsyncKeyState(VK_DOWN) && dir != UP)
	{
		dir = DOWN;
	}
	if (GetAsyncKeyState(VK_LEFT) && dir != RIGHT)
	{
		dir = LEFT;
	}
	if (GetAsyncKeyState(VK_RIGHT) && dir != LEFT)
	{
		dir = RIGHT;
	}
}

void GameManager::inputIntoMap(Maps map,char object, int posX, int posY) {
	map.inputObjectIntoMap(object, posX, posY);
}

void GameManager::move_snake(Maps map, Player snake) {
	
	positionX = 0;
	positionY = 0;

	map.inputObjectIntoMap(' ', positionX, positionY);

	switch (dir)
	{
	case GameManager::UP:
		snake.setHeadPosY = -(snake.getSpeed());
		break;
	case GameManager::DOWN:
		snake.setHeadPosY = (snake.getSpeed());
		break;
	case GameManager::LEFT:
		snake.setHeadPosX = -(snake.getSpeed());
		break;
	case GameManager::RIGHT:
		snake.setHeadPosX = (snake.getSpeed());
		break;
	default:
		break;
	}

	for (int i = & snake.getTotalChild - 1; i > 0 ; --i)
	{
		
	}

}