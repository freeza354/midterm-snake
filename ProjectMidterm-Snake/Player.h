#pragma once
class Player
{
	enum {UP_SNAKE, DOWN_SNAKE, LEFT_SNAKE, RIGHT_SNAKE} dir_snake;

public:
	Player();
	~Player();
	void CollisionDetection(char collision);
	char setKarakterKepala(char karakterKepala);
	char getKarakterKepala();
	char setKarakterAnak(char karakterAnak);
	char getKarakterAnak();
	int setScore(int angka);
	int getScore();
	bool getCanTurn();
	int getHeadPosX();
	int getHeadPosY();
	void setHeadPosX(int x);
	void setHeadPosY(int y);
	int getSpeed();
	int getTotalChild();


private:
	int total_child;
	int score;
	int size;
	int speed;
	char char_child;
	char char_head;
	bool canTurn;
	int posHeadX, posHeadY;
	int posChildX[100], posChildY[100];

};

