#include "pch.h"
#include "Food.h"

Food::Food(char shape, int type, int givenChild)
{
	//1. Good for Player
	//2. Bad for Player

	shape_food = shape;
	type_food = type;
	give_Child = givenChild;

}


Food::~Food()
{
}

int Food::typeGetter() {
	return type_food;
}

int Food::givenChildGetter() {
	return give_Child;
}

char Food::shapeGetter() {
	return shape_food;
}