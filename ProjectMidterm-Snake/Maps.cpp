#include "pch.h"
#include "Maps.h"
#include <iostream>

Maps::Maps()
{
}


Maps::~Maps()
{
}

void Maps::inputCharWall(char tembok) {	
	if (tembok == 'O') {		
		wall = 'O';	
	}
	else
	{
		wall = '#';
	}
}

void Maps::createMap(int x, int y) {

	size_x = x;
	size_y = y;

	for (int i = 0; i < size_y; i++)
	{
		arrayWall[i] = new char[size_x];
	}

	for (int i = 0; i < size_y; i++)
	{
		for (int j = 0; j < size_x; j++)
		{
			//Check column
			if (i == 0 || i == size_y - 1) {
				arrayWall[i][j] = wall;				
			}

			//Check Row
			else if(j == 0 || j == size_x - 1)
			{
				arrayWall[i][j] = wall;
			}

			else
			{
				arrayWall[i][j] = ' ';
			}
		}
	}
}

void Maps::showMap() {

	for (int i = 0; i < size_y; i++)
	{
		for (int j = 0; j < size_x; j++)
		{
			std::cout << arrayWall[i][j];
		}
		std::cout << std::endl;
	}
}

void Maps::createWall(int posX, int posY) {
	
	arrayWall[posX - 1][posY - 1] = wall;

}

void Maps::createWallCollision() {

	//1. Unpassable
	//2. Passable

	for (int i = 0; i < size_y; i++)
	{
		arrayCollision[i] = new int[size_x];
	}

	for (int i = 0; i < size_y; i++)
	{
		for (int j = 0; j < size_x; j++)
		{
			
			if (arrayWall[i][j] == wall)
			{
				arrayCollision[i][j] = 1;
			}
			else
			{
				arrayCollision[i][j] = 2;
			}

		}
	}

}

void Maps::inputObjectIntoMap(char object, int posX, int posY) {
	arrayWall[posX - 1][posY - 1] = object;
}